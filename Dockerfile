FROM adoptopenjdk/openjdk11:jdk-11.0.2.9-slim
VOLUME /tmp
ADD /target/*.jar Service2-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/Service2-0.0.1-SNAPSHOT.jar"]