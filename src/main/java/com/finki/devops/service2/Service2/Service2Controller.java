package com.finki.devops.service2.Service2;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Service2Controller {

  @RequestMapping("/service2")
  public List<String> getAuthors(){
    List<String> authors = new ArrayList<>();
    authors.add("author 1");
    authors.add("author 2");
    return authors;
  }
}
